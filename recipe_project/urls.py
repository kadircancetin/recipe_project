"""recipe_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

from recipes.views import (DeleteRecipeView, HomeView, LikeView, NewRecipeView,
                           RateView, RecipeDetailView, RecipeSearchView,
                           RecipesFromIngradientView, UpdateRecipeView)
from users.views import SignUp

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", HomeView.as_view(), name="home"),
    path(
        "ingradient/<int:pk>/", RecipesFromIngradientView.as_view(), name="ingradient"
    ),
    path("recipe/<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("search/", RecipeSearchView.as_view(), name="search"),
    path("delete_recipe/<int:pk>/", DeleteRecipeView.as_view(), name="delete_recipe"),
    path("signup/", SignUp.as_view(), name="signup"),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="users/login.html"),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("new_recipe/", NewRecipeView.as_view(), name="new_recipe"),
    path("update_recipe/<int:pk>/", UpdateRecipeView.as_view(), name="update_recipe"),
    path("like/<int:pk>/", LikeView.as_view(), name="like"),
    path("rate/<int:pk>/", RateView.as_view(), name="rate"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
