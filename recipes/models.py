from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Avg
from django.db.models.functions import Coalesce
from django.utils.translation import gettext as _


# MANAGERS
class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


# MODELS
class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey("recipes.Recipe", on_delete=models.CASCADE)


class Rate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey("recipes.Recipe", on_delete=models.CASCADE)
    rate = models.IntegerField(
        validators=[MaxValueValidator(100), MinValueValidator(1)]
    )


class Recipe(models.Model):
    class DIFFICULTIES(models.IntegerChoices):
        EASY = 1, _("Easy")
        MEDIUM = 2, _("Medium")
        HARD = 3, _("Hard")

    title = models.CharField(max_length=255, verbose_name=_("title"))
    description = models.TextField(verbose_name=_("description"))
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_("create time"))
    change_time = models.DateTimeField(auto_now=True, verbose_name=_("change time"))
    view_count = models.IntegerField(default=0)
    difficulty = models.IntegerField(
        choices=DIFFICULTIES.choices, verbose_name=_("difficulty")
    )
    image = models.ImageField()

    # Relations with other models
    author = models.ForeignKey(
        User, null=True, on_delete=models.SET_NULL, verbose_name=_("author")
    )
    ingredients = models.ManyToManyField("recipes.Ingradient")
    likes = models.ManyToManyField(
        User, through=Like, related_name="likes_set", through_fields=("recipe", "user")
    )
    rates = models.ManyToManyField(
        User, through=Rate, through_fields=("recipe", "user"), related_name="rates_set"
    )
    is_active = models.BooleanField(default=True)

    # Managers
    objects = models.Manager()
    actives = IsActiveManager()

    @property
    def rate_avg(self):
        rates = Rate.objects.filter(recipe=self.pk)
        return rates.aggregate(rate__avg=Coalesce(Avg("rate"), 0))["rate__avg"]

    @property
    def rate_count(self):
        return self.rates.count()

    @property
    def like_count(self):
        return self.likes.count()

    def delete(self):
        self.is_active = False
        self.save()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Recipe")
        verbose_name_plural = _("Recipes")


class Ingradient(models.Model):
    ingredient = models.CharField(max_length=128, verbose_name=_("ingredient"))

    def __str__(self):
        return self.ingredient
