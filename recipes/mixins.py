from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext as _


class OwnerPermissionMixin:
    def has_permissions(self):
        if not self.request.user.is_authenticated:
            return False
        return self.get_object().author == self.request.user

    def dispatch(self, request, *args, **kwargs):

        if not self.has_permissions():
            raise PermissionDenied(_("You do not have permission."))
        return super().dispatch(request, *args, **kwargs)
