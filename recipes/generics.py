from django.core.paginator import Paginator
from django.db.models import Count
from django.shortcuts import render
from django.views import View

from recipes.models import Recipe


class GenericRecipeListView(View):
    page_content_count = 2

    def get_recipe_queryset(self):
        return self.recipe_queryset

    def get(self, request, *args, **kwargs):
        query_set = self.get_recipe_queryset().order_by("-create_time")

        paginator = Paginator(query_set, self.page_content_count)
        recipes = paginator.get_page(request.GET.get("page", 1))

        context = {
            "recipes": recipes,
            "most_used_ingredients": (
                Recipe.actives.all()
                .values("ingredients__ingredient", "ingredients__pk")
                .annotate(total=Count("ingredients"))
                .order_by("-total")[:5]
            ),
        }
        return render(request, "recipes/recipe_list.html", context)
