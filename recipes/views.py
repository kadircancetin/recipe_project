from django.contrib.auth.decorators import login_required
from django.db.models import F, Q
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import CreateView, DeleteView, UpdateView

from recipes.forms import RecipesForm
from recipes.generics import GenericRecipeListView
from recipes.mixins import OwnerPermissionMixin
from recipes.models import Ingradient, Like, Rate, Recipe


class HomeView(GenericRecipeListView):
    recipe_queryset = Recipe.actives.all()


class RecipesFromIngradientView(GenericRecipeListView):
    def get_recipe_queryset(self):
        return Recipe.actives.filter(ingredients__pk=self.kwargs.get("pk"))


class RecipeSearchView(GenericRecipeListView):
    def get_recipe_queryset(self):
        # get searched ingradients
        search_words = [x.strip() for x in self.request.GET.get("q").split(",")]
        or_queris = Q()
        for words in search_words:
            or_queris = or_queris | Q(ingredient__icontains=words)
        ingrads = Ingradient.objects.filter(or_queris)

        # get recipes which has all the ingradient in same time
        recipes = Recipe.objects.none()
        if ingrads.count() != 0:
            recipes = ingrads.first().recipe_set.all()
            for ingrad in ingrads[1:]:
                recipes = recipes.intersection(ingrad.recipe_set.all())

        # search from title
        if recipes.count() == 0:
            or_queris = Q()
            for words in search_words:
                or_queris = or_queris | Q(title__icontains=words)
                or_queris = or_queris | Q(description__icontains=words)
            recipes = Recipe.objects.filter(or_queris)

        return recipes


class DeleteRecipeView(OwnerPermissionMixin, DeleteView):
    model = Recipe
    success_url = reverse_lazy("home")


@method_decorator(login_required, name="dispatch")
class NewRecipeView(CreateView):
    model = Recipe
    form_class = RecipesForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


@method_decorator(login_required, name="dispatch")
class UpdateRecipeView(OwnerPermissionMixin, UpdateView):
    model = Recipe
    form_class = RecipesForm

    def form_valid(self, form):
        obj = form.save(commit=True)
        return redirect("recipe_detail", obj.id)


class RecipeDetailView(View):
    def get(self, request, *args, **kwargs):
        self.recipe = get_object_or_404(Recipe, pk=kwargs.get("pk"), is_active=True)
        self.recipe.view_count = F("view_count") + 1
        self.recipe.save()

        context = {"recipe": self.recipe, **self.__get_liked_and_rate()}
        return render(request, "recipes/recipe_detail.html", context)

    def __get_liked_and_rate(self):
        if not self.request.user.is_authenticated:
            return {}

        is_rated = True
        rate_point = 0
        try:
            rate_point = Rate.objects.get(
                user=self.request.user, recipe=self.recipe
            ).rate
        except Rate.DoesNotExist:
            is_rated = False

        return {
            "is_liked": Like.objects.filter(user=self.request.user, recipe=self.recipe),
            "is_rated": is_rated,
            "rate_point": rate_point,
        }


# User interaction Views


@method_decorator(login_required, name="dispatch")
class LikeView(View):
    def get(self, request, pk):
        recipe = get_object_or_404(Recipe, pk=pk)
        try:
            Like.objects.get(user=request.user, recipe=recipe).delete()
        except Like.DoesNotExist:
            Like.objects.create(user=request.user, recipe=recipe)

        return redirect("recipe_detail", pk=pk)


@method_decorator(login_required, name="dispatch")
class RateView(View):
    def __validate(self, pk):
        rate = int(self.request.POST.get("rate"))
        if not rate or rate < 0 or rate > 5:
            return redirect("recipe_detail", pk=pk)

    def post(self, request, pk):
        recipe = get_object_or_404(Recipe, pk=pk)
        self.__validate(pk)

        Rate.objects.update_or_create(
            user=request.user,
            recipe=recipe,
            defaults={"rate": request.POST.get("rate")},
        )

        return redirect("recipe_detail", pk=pk)
