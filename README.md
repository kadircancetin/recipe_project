This is a basic django backend app that you can share recipe and read, rate, delete the shared recipes. This app created for [hipo labs](https://hipolabs.com/) backend intern application chalange.

You can look the project from [https://glacial-dawn-98828.herokuapp.com](https://glacial-dawn-98828.herokuapp.com/).

# Installation

```
pip install -r requirements.txt;
python manage.py migrate;
python manage.py runserver;
```

